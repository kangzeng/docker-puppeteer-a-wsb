# Changelog

## v5.0.1 (2021-11-03)

### Fixed

- Add current OS version to image tags in `.gitlab-ci.yml` to identify them explicitly (#24)

### Miscellaneous

- Update renovate config to remove `gitlabci-include` override, which is now part of renovate (as of `28.15.0`) (#30)

## v5.0.0 (2021-10-27)

### Changed

- BREAKING: Updated `latest` image to Node 16, which is now the active LTS release. (#27)

### Miscellaneous

- Updated `renovate` config to include tracking for all Docker template `include`s (#29)

## v4.2.0 (2021-10-20)

### Added

- Added container image for Node 17, uses Debian Buster due to deprecated library in Debian Bullseye, which is the default for Node 17 (#26, #28).

### Changed

- Augmented container test with screenshot to verify proper page load (#25)

### Miscellaneous

- Add [`gitlab-releaser`](https://gitlab.com/gitlab-ci-utils/gitlab-releaser) file to set milestones for releases.

## v4.1.0 (2021-10-10)

### Changed

- Moved to container build/test/deploy pipeline leveraging `kaniko` for build and `skopeo` for deploy. (#20)

### Miscellaneous

- Setup [renovate](https://docs.renovatebot.com/) for dependency updates, including configuring tracking Node versions in CI pipeline (#19)
- Add simple puppeteer test with image before deploy (#18)

## v4.0.0 (2021-06-02)

### Changed

- BREAKING: Deprecated Node 15 support since end-of-life. Compatible with all current and LTS releases (`^12.20.0 || ^14.15.0 || >=16.0.0`). (#14)

## v3.0.0 (2021-05-01)

### Changed

- BREAKING: Deprecate Node 10 support since end-of-life (#15)

## v2.2.0 (2021-04-27)

### Added

- Added container image for Node v16 (#13)

### Changed

- Updated Dockerfile to add libgbm1 to resolve Chrome launching issue in Node v16 (#13)

### Miscellaneous

- Updated CI pipeline to leverage simplified include syntax in GitLab 13.6 (#9), use matrix syntax for Node versions (#11), and clean up `latest` tag for Node LTS release (#12)

## v2.1.0 (2020-10-27)

### Changed

- Updated Node LTS from v12 to v14. (#5)

## v2.0.0 (2020-10-23)

### Changed

- BREAKING: Remove version numbered images. (#8)

## v1.2.0 (2020-10-23)

### Added

- Add image for Node 15 (#6)

### Changed

- Update pipeline to build all images by leveraging a common template, using ARG in Dockerfile to specify version, and spawning child pipelines for each image. (#7)

## v1.1.0 (2020-06-03)

### Changed

- Removed Node 13 references and scheduled build since end-of-life as of 2020-06-01 (#3)

## v1.0.0 (2019-09-28)

### Initial Release

- Initial container image with Puppeteer's recommended configuration.
