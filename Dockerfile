# Based on Google Puppeteer's recommended Dockerfile, now only a historical version:
# https://github.com/puppeteer/puppeteer/blob/v3.0.1/.ci/node12/Dockerfile.linux

ARG IMAGE_TAG=lts

FROM node:$IMAGE_TAG

# Ignore rule to pin package versions.  Given the number of dependencies this isn't feasible,
# and want the the latest to keep up with Chromium/Puppeteer changes.
# hadolint ignore=DL3008
RUN apt-get update && \
  apt-get -y install --no-install-recommends xvfb gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 \
  libdbus-1-3 libexpat1 libfontconfig1 libgbm1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 \
  libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 \
  libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 \
  libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget && \
  rm -rf /var/lib/apt/lists/*

# Add user to avoid --no-sandbox (recommended)
RUN groupadd -r pptruser && useradd -r -g pptruser -G audio,video pptruser \
  && mkdir -p /home/pptruser/Downloads \
  && chown -R pptruser:pptruser /home/pptruser

# Run everything after as non-privileged user.
USER pptruser
