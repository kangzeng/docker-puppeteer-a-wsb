# Docker Puppeteer

## About Docker Puppeteer

Docker Puppeteer is a container image with the recommended configuration for using [Puppeteer](https://github.com/GoogleChrome/puppeteer) to drive a headless Chrome browser (from the Puppeteer project).

**Notes:**

- This image does not install Puppeteer itself.  It is assumed Puppeteer will be installed by the appropriate application using the container.
- This image runs under a non-privileged account, so Puppeteer may fail to install globally (see https://github.com/GoogleChrome/puppeteer/issues/375 for details and workarounds).
- In order for Chrome to launch properly in this container in some configurations, including running this Linux container under Docker for Windows, the `--no-sandbox` argument must be passed to Puppeteer.

## Docker Puppeteer Images

All available Docker image tags can be found in the `gitlab-ci-utils/docker-puppeteer` repository at https://gitlab.com/gitlab-ci-utils/docker-puppeteer/container_registry.  Details on each release can be found on the [Releases](https://gitlab.com/gitlab-ci-utils/docker-puppeteer/releases) page.

The following tag conventions are used:

- `latest`, `node-14`: Latest release built on the current Node active LTS release (Node 14, rebuilt weekly)
- `node-12`: Latest release built on the current Node 12 release (rebuilt weekly)
- `node-16`: Latest release built on the current Node 16 release (rebuilt weekly)

**Note:** Any images in the `gitlab-ci-utils/docker-puppeteer/tmp` repository are temporary images used during the build process and may be deleted at any point.
